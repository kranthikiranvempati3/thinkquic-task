import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadExcelComponent } from './upload-excel/upload-excel.component';

const routes: Routes = [
  {
    path:"",
    component:UploadExcelComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
